Source: golang-github-qor-inflection
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
# TODO Uploaders:
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-go
Standards-Version: 3.9.6
Homepage: https://github.com/qor/inflection
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-qor-inflection
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-qor-inflection.git
XS-Go-Import-Path: github.com/qor/inflection
Testsuite: autopkgtest-pkg-go

Package: golang-github-qor-inflection-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: foreign
Description: Pluralizes and singularizes English nouns
 Inflection is a Go package that pluralizes and singularizes English nouns.
 .
   inflection.Plural("person") => "people"
   inflection.Plural("Person") => "People"
   inflection.Plural("PERSON") => "PEOPLE"
   inflection.Plural("bus")    => "buses"
   inflection.Plural("BUS")    => "BUSES"
   inflection.Plural("Bus")    => "Buses"
 .
   inflection.Singularize("people") => "person"
   inflection.Singularize("People") => "Person"
   inflection.Singularize("PEOPLE") => "PERSON"
   inflection.Singularize("buses")  => "bus"
   inflection.Singularize("BUSES")  => "BUS"
   inflection.Singularize("Buses")  => "Bus"
 .
   inflection.Plural("FancyPerson") => "FancyPeople"
   inflection.Singularize("FancyPeople") => "FancyPerson"
 .
 Standard rules are from Rails's ActiveSupport.
 .
 This package contains the source.
